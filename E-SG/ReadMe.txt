To compile:
	javac -d build src/*.java

	
To execute experiment with 4 light agents:
	java -cp build src.MainLight
	
	
To execute experiment with 1 heavy and 3 light agents:
	java -cp build src.MainHeavy

Compiled and executed with Java version: openjdk version "17.0.12" 2024-07-16

Results folder will contain the output files, one file with the results of the tests and another file with timestamps.
