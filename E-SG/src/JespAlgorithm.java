package src;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JespAlgorithm {

    private Map<Agent, Map<String, Double>> agentsQHarmAtInitialState;
    private Map<Agent, Map<String, Double>> agentsQGoodAtInitialState;

    public JointPolicy exhaustiveJESP(LexVIAlgorithm lexVi,  Map<State, Map<Agent, String>> jointPolicy, State initialState, Double empathyCoef){
        
        int conv = 0;
        List<Agent> agents = lexVi.getAllAgents();
        
        this.agentsQHarmAtInitialState = new HashMap<>();
        this.agentsQGoodAtInitialState = new HashMap<>();
        

        JointPolicy prevP = new JointPolicy(jointPolicy);
        JointPolicy newP = new JointPolicy(jointPolicy);

        while(conv <= agents.size() - 1){
            // Iterate over all agents
            for(Agent agent : agents){
                prevP = lexVi.calculatePolicyValue(prevP, agent, empathyCoef);

                newP = lexVi.calculateBestResponsePolicySuccessiveVI(prevP, agent, empathyCoef);

                String newPInitialAction = newP.getFixedPolicy().get(initialState).get(agent);
                String prevPInitialAction = prevP.getFixedPolicy().get(initialState).get(agent);

                Double newHarmInitState = newP.getqHarm().get(initialState).get(newPInitialAction);
                Double prevHarmInitState = prevP.getqHarm().get(initialState).get(prevPInitialAction);
                
                Double newGoodInitState = newP.getqGood().get(initialState).get(newPInitialAction);
                Double prevGoodInitState = prevP.getqGood().get(initialState).get(prevPInitialAction);
                // If the calculated policy has les harm or has more good if it has equal harm than the previous policy, it becomes the previous policy
                if((newHarmInitState > prevHarmInitState) || (newHarmInitState == prevHarmInitState && newGoodInitState > prevGoodInitState)){
                    prevP = new JointPolicy(newP);
                    conv = 0;
                }else{
                    conv++;
                }
                agentsQHarmAtInitialState.put(agent, prevP.getqHarm().get(initialState));
                agentsQGoodAtInitialState.put(agent, prevP.getqGood().get(initialState));
            
                if(conv == agents.size()-1){
                    break;
                }
            }
        }
        return prevP;
    }

    public Map<Agent, Map<String, Double>> getAgentsQHarmAtInitialState() {
        return agentsQHarmAtInitialState;
    }

    public Map<Agent, Map<String, Double>> getAgentsQGoodAtInitialState() {
        return agentsQGoodAtInitialState;
    }
    
    
    
}
