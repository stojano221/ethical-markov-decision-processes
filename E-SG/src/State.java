package src;
import java.util.Map;
import java.util.Objects;
public class State {


    // Product -> Quantity
    private Map<String, Integer> products;

    // Agent -> Product -> Quantity
    private Map<Agent, Map<String, Integer>> agentsHave;

    private String stateString;

    public State(Map<String, Integer> products, Map<Agent, Map<String, Integer>> agentsHave) {
        this.products = products;
        this.agentsHave = agentsHave;
        stateString = this.createStateString();
    }

    private String createStateString(){

        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        for(Map.Entry<String, Integer> p : products.entrySet()){
            builder.append("P_"+ p.getKey()+ ",Qp:" + p.getValue() + ";");
        }
        
        for(Map.Entry<Agent, Map<String, Integer>> agentHas : agentsHave.entrySet()){
            builder.append("Ag_" + agentHas.getKey().getAgentNumber() + ":");

            for(Map.Entry<String,Integer> has : agentHas.getValue().entrySet()){
                builder.append("P_"+ has.getKey()+ ",Qp:" + has.getValue()+ ";");
            }
        }

        return builder.toString();
    }

    public Map<String, Integer> getProducts() {
        return products;
    }

    public Map<Agent, Map<String, Integer>> getAgentsHave() {
        return agentsHave;
    }

    public String getStateString() {
        return stateString;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        for(Map.Entry<String, Integer> p : products.entrySet()){
            builder.append("P_"+ p.getKey()+ ", Qp: " + p.getValue() + "\n");
        }

        for(Map.Entry<Agent, Map<String, Integer>> agentHas : agentsHave.entrySet()){
            builder.append("Ag_" + agentHas.getKey().getAgentNumber() + "{ ");

            for(Map.Entry<String,Integer> has : agentHas.getValue().entrySet()){
                builder.append("P_"+ has.getKey()+ ", Qp: " + has.getValue() + " ; ");
            }
            builder.append("} \n");
        }

        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(stateString);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;  
        } 
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        State s = (State) obj;

        return this.getStateString().equals(s.getStateString());
    }
}

