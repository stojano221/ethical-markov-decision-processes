package src;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StarvationEvaluator implements Evaluator{
    
    private Double evaluation;

    StarvationEvaluator(Double evaluation){
        this.evaluation = evaluation;
    }

    @Override
    public Double evaluate(State state, Map<Agent, String> jointAction, State successorState, Agent agent ) {

        String takenProduct = jointAction.get(agent);
        List<String> possibleActions = Utility.calculateActionsForState(state);
        
        Integer agentHasProductState = 0;
        Integer agentHasProductSuccessorState = 0;
        boolean productObtained = false;

        // The agent did not that a necessity product while it was possible to do so
        List<String> possibleActionsNecessity = new ArrayList<String>();
        Integer minDemandForTakenProduct;

        // It was possible to try to take a necessity product
        for(String action : possibleActions){
            if(action.startsWith("n") ){
                minDemandForTakenProduct = agent.getMinDemand().get(action);
                agentHasProductState = state.getAgentsHave().get(agent).get(action);
                if(agentHasProductState < minDemandForTakenProduct){
                    possibleActionsNecessity.add(action);
                }
            }
        }

        // The agent obtained the product
        if(!takenProduct.equals("NO_PRODUCT")){
            agentHasProductState = state.getAgentsHave().get(agent).get(takenProduct);
            agentHasProductSuccessorState = successorState.getAgentsHave().get(agent).get(takenProduct);
            productObtained = agentHasProductState + 1 == agentHasProductSuccessorState;

        }

        if ((possibleActionsNecessity.size() > 0) && !(possibleActionsNecessity.contains(takenProduct) || productObtained)) {
            return evaluation;
        }

        return 0.0;

    }
}
