package src;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Agent {
    
    private Integer agentNumber;
    private String type;

    private Map<String, Integer> minDemand;
    private Map<String, Integer> maxDemand;

    private Map<String, Integer> context;
    private Map<String, Evaluator> evaluatorMap;

    private Integer goodContextSize;
    private Integer badContextSize;

    public Agent(Integer agentNumber, String type, Map<String, Integer> minDemand, Map<String, Integer> maxDemand, Map<String, Integer> context, Map<String, Evaluator> evaluatorMap) {
        this.agentNumber = agentNumber;
        this.type = type;
        this.minDemand = minDemand;
        this.maxDemand = maxDemand;
        this.context = context;
        this.evaluatorMap = evaluatorMap;
        setUpContextSizes(context);
    }

    public Agent(Integer agentNumber, String type, Map<String, Integer> minDemand, Map<String, Integer> maxDemand) {
        this.agentNumber = agentNumber;
        this.type = type;
        this.minDemand = minDemand;
        this.maxDemand = maxDemand;
    }

    public Agent(){
        this(-100, "MT", new HashMap<>(), new HashMap<>());
    }

    public Double evaluateTransitionForValue(State state, Map<Agent, String> jointAction, State successorState, String valueName){

        return this.evaluatorMap.get(valueName).evaluate(state, jointAction, successorState, this);
    }

    private void setUpContextSizes(Map<String, Integer> context){
        Integer goodSize = 0;
        Integer badSize = 0;
        for(Map.Entry<String, Integer> contextEntry : context.entrySet()){
            if(contextEntry.getValue() > 0){
                goodSize++;
            }else if(contextEntry.getValue() < 0){
                badSize++;
            }
        }

        this.goodContextSize = goodSize;
        this.badContextSize = badSize;
    }

    public Integer getGoodContextSize() {
        return goodContextSize;
    }

    public Integer getBadContextSize() {
        return badContextSize;
    }

    public Integer getAgentNumber() {
        return agentNumber;
    }

    public String getType() {
        return type;
    }
    
    public Map<String, Integer> getMinDemand() {
        return minDemand;
    }

    public Integer getMinDemandForP(String p) {
        return minDemand.get(p);
    }

    public Map<String, Integer> getMaxDemand() {
        return maxDemand;
    }

    public Integer getMaxDemandForP(String p) {
        return maxDemand.get(p);
    }

    public Map<String, Integer> getContext() {
        return context;
    }

    public Map<String, Evaluator> getEvaluatorMap() {
        return evaluatorMap;
    }

    @Override
    public String toString() {
        // return "Agent: " + getAgentNumber() + " ;Type: " + getType();
        return "Ag" + getAgentNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.toString());
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;  
        } 
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Agent a = (Agent) obj;

        return this.toString().equals(a.toString());
    }
}
