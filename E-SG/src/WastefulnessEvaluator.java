package src;
import java.util.Map;

public class WastefulnessEvaluator implements Evaluator {

    private Double evaluation;

    WastefulnessEvaluator(Double evaluation){
        this.evaluation = evaluation;
    }

    @Override
    public Double evaluate(State state, Map<Agent, String> jointAction, State successorState, Agent agent) {
        
        Double res = 0.0;

        String takenProduct = jointAction.get(agent);

        if(!takenProduct.equals("NO_PRODUCT")){
    
            // A product has been removed from the shelf
            Integer productState = state.getProducts().get(takenProduct);
            Integer productSuccessorState = successorState.getProducts().get(takenProduct);
            boolean stateProductRemoved = productState - 1 == productSuccessorState;
    
            // At least one agent has obtained the product
            Integer agentHasProductState;
            Integer agentHasProductSuccessorState;
            boolean productNotDestroyed = false;
            for (Map.Entry<Agent, String> action : jointAction.entrySet()) {
                agentHasProductState = state.getAgentsHave().get(action.getKey()).get(takenProduct);
                agentHasProductSuccessorState = successorState.getAgentsHave().get(action.getKey()).get(takenProduct);
                if (agentHasProductState + 1 == agentHasProductSuccessorState) {
                    productNotDestroyed = true;
                    break;
                }
            }
            // The agent was involved in a product destruction: stock has gone down but no agent has obtained it.
            if(stateProductRemoved && !productNotDestroyed){
                res = this.evaluation; 
            }
        }
        return res;
    }
    
}
