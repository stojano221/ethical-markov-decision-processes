package src;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RewardCalculator {
    

    public static EthicalRewardQuad individualReward(State state, Map<Agent, String> jointAction, State successorState, Agent agent){

        Double causedHarm = 0.0;
        Double causedGood = 0.0;
        Double repairedHarm = 0.0;
        Double repairedGood = 0.0;

        Double causedHarmSum = 0.0;
        Double causedGoodSum = 0.0;
        Double repairedHarmSum = 0.0;
        Double repairedGoodSum = 0.0;

        Map<String, Integer> context = agent.getContext();

        for(Map.Entry<String, Integer> contextEntry : context.entrySet()){

            Double evaluationForValue = agent.evaluateTransitionForValue(state, jointAction, successorState, contextEntry.getKey());

            // Calculate the sum of the four elements for the values of the context of the given agent
            if(contextEntry.getValue() > 0){
                if(evaluationForValue > 0){
                    causedGoodSum += contextEntry.getValue() * evaluationForValue;
                }else if(evaluationForValue < 0){
                    repairedGoodSum += contextEntry.getValue() * Math.abs(evaluationForValue);
                }
            }else if(contextEntry.getValue() < 0){
                if(evaluationForValue > 0){
                    causedHarmSum += Math.abs(contextEntry.getValue()) * evaluationForValue;
                }else if(evaluationForValue < 0){
                    repairedHarmSum += Math.abs(contextEntry.getValue()) * Math.abs(evaluationForValue);
                }
            }

        }
        // Calculate the moral worth by definition
        causedHarm   = causedHarmSum;
        repairedHarm = repairedHarmSum;

        causedGood   = causedGoodSum;
        repairedGood = repairedGoodSum;

        return new EthicalRewardQuad(causedHarm, repairedHarm, causedGood, repairedGood);
    }

    // Multiply ethical reward quads into one
    public static EthicalRewardQuad aggregationFunction(Map<Agent, EthicalRewardQuad> agentsRewards){
        Double causedHarmProd = 1.0;
        Double repairedHarmProd = 1.0;
        Double causedGoodProd = 1.0;
        Double repairedGoodProd = 1.0;

        for(Map.Entry<Agent, EthicalRewardQuad> rewardEntry : agentsRewards.entrySet()){

            EthicalRewardQuad rewardQuad = rewardEntry.getValue();

            causedHarmProd   *= 1 + rewardQuad.getCausedHarm();
            repairedHarmProd *= 1 + rewardQuad.getRepairedHarm();
            causedGoodProd *= 1 + rewardQuad.getCausedGood();
            repairedGoodProd *= 1 + rewardQuad.getRepairedGood();
        }

        return new EthicalRewardQuad(causedHarmProd, repairedHarmProd, causedGoodProd, repairedGoodProd);

    }

    // Aggregate reward
    public static EthicalRewardQuad aggregateRewardForAgent(State state, Map<Agent, String> jointAction, State successorState, 
                                                            List<Agent> allAgents, Agent individualAgent, Double empathyCoef){
        Map<Agent, EthicalRewardQuad> agentsRewards = new HashMap<>();
        
        for(Agent ag : allAgents){
            agentsRewards.put(ag, individualReward(state, jointAction, successorState, ag));
        }

        EthicalRewardQuad individualAgentQuad = agentsRewards.get(individualAgent);
        Double individualCausedHarm = individualAgentQuad.getCausedHarm();
        Double individualRepairedHarm = individualAgentQuad.getRepairedHarm();

        Double individualCausedGood = individualAgentQuad.getCausedGood();
        Double individualRepairedGood = individualAgentQuad.getRepairedGood();

        // Make the set of J\i
        Map<Agent, EthicalRewardQuad> otherAgentsRewards = new HashMap<>(agentsRewards);
        otherAgentsRewards.remove(individualAgent);

        // Calculate their product
        EthicalRewardQuad aggregatedQuadOther = aggregationFunction(otherAgentsRewards);
        
        // Calculate the aggregate reward

        Double resultCausedHarm   = individualCausedHarm + (1 - empathyCoef) * (individualCausedHarm)  + empathyCoef * aggregatedQuadOther.getCausedHarm();
        Double resultRepairedHarm = individualRepairedHarm + (1 - empathyCoef) * (individualRepairedHarm) + empathyCoef * aggregatedQuadOther.getRepairedHarm();
        Double resultCausedGood   = individualCausedGood + (1 - empathyCoef) * (individualCausedGood) + empathyCoef * aggregatedQuadOther.getCausedGood();
        Double resultRepairedGood = individualRepairedGood + (1 - empathyCoef) * (individualRepairedGood) + empathyCoef * aggregatedQuadOther.getRepairedGood();
        

        return new EthicalRewardQuad(resultCausedHarm, resultRepairedHarm, resultCausedGood, resultRepairedGood);

    }

    
}
