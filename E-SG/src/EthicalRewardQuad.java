package src;
public class EthicalRewardQuad {

    private Double causedHarm;
    private Double repairedHarm;
    private Double causedGood;
    private Double repairedGood;

    public EthicalRewardQuad(Double causedHarm, Double repairedHarm, Double causedGood, Double repairedGood) {
        this.causedHarm = causedHarm;
        this.repairedHarm = repairedHarm;
        this.causedGood = causedGood;
        this.repairedGood = repairedGood;
    }

    public void setCausedHarm(Double causedHarm) {
        this.causedHarm = causedHarm;
    }

    public void setRepairedHarm(Double repairedHarm) {
        this.repairedHarm = repairedHarm;
    }

    public void setCausedGood(Double causedGood) {
        this.causedGood = causedGood;
    }

    public void setRepairedGood(Double repairedGood) {
        this.repairedGood = repairedGood;
    }

    public Double getCausedHarm() {
        return causedHarm;
    }

    public Double getRepairedHarm() {
        return repairedHarm;
    }

    public Double getCausedGood() {
        return causedGood;
    }

    public Double getRepairedGood() {
        return repairedGood;
    }

    @Override
    public String toString() {
        return "CausedHarm : " + causedHarm + " , RepairedHarm : " + repairedHarm + " , CausedGood : " + causedGood
                + " , RepairedGood : " + repairedGood;
    }
}
