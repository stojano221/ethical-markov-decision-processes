package src;
import java.util.HashMap;
import java.util.Map;

public class JointPolicy {

    private Map<State, Map<Agent, String>> fixedPolicy;

    private Map<State, Map<String, Double>> qHarm;
    private Map<State, Map<String, Double>> qGood;

    public JointPolicy(Map<State, Map<Agent, String>> jointPolicy){
        this.fixedPolicy = jointPolicy;
        this.qHarm = getZeroQvalue();
        this.qGood = getZeroQvalue();
    }

    public JointPolicy(Map<State, Map<Agent, String>> jointPolicy, Map<State, Map<String, Double>> qHarm, Map<State, Map<String, Double>> qGood){
        this.fixedPolicy = jointPolicy;
        this.qHarm = qHarm;
        this.qGood = qGood;
    }

    public JointPolicy(JointPolicy jointPolicy){
        this.fixedPolicy = deepCopyFixedPolicy(jointPolicy.getFixedPolicy());
        this.qHarm = deepCopyQValue(jointPolicy.getqHarm());
        this.qGood = deepCopyQValue(jointPolicy.getqGood());
    }

    private Map<State, Map<String,  Double>> getZeroQvalue(){
        Map<State, Map<String,  Double>> qValue = new HashMap<>();

        // Initialise qValue to 0 for all state-action pairs for the unfixed agent
        for(State state : fixedPolicy.keySet()){
            Map<String, Double> tempActionHarm = new HashMap<>();
            for(String action : Utility.calculateActionsForState(state)){
                tempActionHarm.put(action, 0.0);
            }
            qValue.put(state, tempActionHarm);
        }
        return qValue;
    }

    public Map<State, Map<Agent, String>> getFixedPolicy() {
        return fixedPolicy;
    }

    public Map<State, Map<String, Double>> getqHarm() {
        return qHarm;
    }

    public Map<State, Map<String, Double>> getqGood() {
        return qGood;
    }

    public void setqHarm(Map<State, Map<String, Double>> qHarm) {
        this.qHarm = qHarm;
    }

    public void setqGood(Map<State, Map<String, Double>> qGood) {
        this.qGood = qGood;
    }

    public void setFixedPolicy(Map<State, Map<Agent, String>> fixedPolicy) {
        this.fixedPolicy = fixedPolicy;
    }

    

    public  Map<State, Map<Agent, String>> deepCopyFixedPolicy( Map<State, Map<Agent, String>> fixedPolicy){
        Map<State, Map<Agent, String>> copy = new HashMap<>();
        for(Map.Entry<State, Map<Agent, String>> policyEntry: fixedPolicy.entrySet()){
            Map<Agent, String> agentAction = new HashMap<>();
            for(Map.Entry<Agent, String> agentActionEntry : policyEntry.getValue().entrySet()){
                agentAction.put(agentActionEntry.getKey(), agentActionEntry.getValue());
            }
            copy.put(policyEntry.getKey(), agentAction);
        }
        return copy;
    }

    public Map<State, Map<String, Double>> deepCopyQValue(Map<State, Map<String, Double>> qValue){
        Map<State, Map<String, Double>> copy = new HashMap<>();
        for(Map.Entry<State, Map<String, Double>> valueEntry: qValue.entrySet()){
            Map<String, Double> actionDouble = new HashMap<>();
            for(Map.Entry<String, Double> actionDoubleEntry : valueEntry.getValue().entrySet()){
                actionDouble.put(actionDoubleEntry.getKey(), actionDoubleEntry.getValue());
            }
            copy.put(valueEntry.getKey(), actionDouble);
        }
        return copy;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        for(Map.Entry<State, Map<Agent, String>> polEntry : fixedPolicy.entrySet()){
            sb.append("State " + polEntry.getKey() + ":");
            for(Map.Entry<Agent,String> agentActionsEntry : polEntry.getValue().entrySet()){
                sb.append(agentActionsEntry.getKey() + " : " + agentActionsEntry.getValue() + "\n");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    
    
}
