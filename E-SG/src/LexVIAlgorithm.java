package src;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LexVIAlgorithm {
    
    private List<Agent> allAgents;
    private Double convergenceAchieved;
    private Double gamma;
    private Double epsilonCausedHarm;
    private Double epsilonRepairedGood;

    
    private MaximizingPolicyExtractor maxPolicyExtractor;

    public LexVIAlgorithm(List<Agent> allAgents, 
                                    Double gamma, Double convergenceAchieved,  
                                    Double epsilonCausedHarm, Double epsilonRepairedGood){
        this.allAgents = allAgents;
        this.gamma = gamma;
        this.convergenceAchieved = convergenceAchieved;
        this.epsilonCausedHarm = epsilonCausedHarm;
        this.epsilonRepairedGood = epsilonRepairedGood;
        this.maxPolicyExtractor = new MaximizingPolicyExtractor();

    }

    public JointPolicy calculateBestResponsePolicySuccessiveVI(JointPolicy jointPolicy, Agent unfixedAgent, Double empathyCoef){

        Map<State, Map<Agent, String>> fixedPolicy = jointPolicy.deepCopyFixedPolicy(jointPolicy.getFixedPolicy());

        Map<State, List<String>> unfixedAgentPolicy = getUnfixedPolicyForAgent(fixedPolicy);

        // State -> Agent -> Action -> qValue
        // Calculate values for harm
        Map<State, Map<String, Double>> qHarm = calculateQValues(fixedPolicy, unfixedAgentPolicy, unfixedAgent, empathyCoef, true);

        Map<State, List<String>> unfixedAgentHarmPolicy = maxPolicyExtractor.extract(unfixedAgentPolicy, qHarm);
        // Calculate values for good
        Map<State, Map<String, Double>> qGood = calculateQValues(fixedPolicy, unfixedAgentHarmPolicy, unfixedAgent, empathyCoef, false);
        
        Map<State, List<String>> unfixedAgentGoodPolicy = maxPolicyExtractor.extract(unfixedAgentHarmPolicy, qGood);
        Map<State, Map<Agent, String>> newJointPolicy = getJointPolicy(fixedPolicy, unfixedAgentGoodPolicy, unfixedAgent);
        JointPolicy resultJointP = new JointPolicy(newJointPolicy, qHarm, qGood);

        return resultJointP;
    }

    public JointPolicy calculatePolicyValue(JointPolicy jointPolicy, Agent unfixedAgent, Double empathyCoef){

        Map<State, Map<Agent, String>> fixedPolicy = jointPolicy.getFixedPolicy();

        Map<State, List<String>> unfixedAgentPolicy = getPlaceholderUnfixedPolicy(fixedPolicy, unfixedAgent);

        // State -> Agent -> Action -> qValue
        // Calculate values for harm
        Map<State, Map<String, Double>> qHarm = calculateQValues(fixedPolicy, unfixedAgentPolicy, unfixedAgent, empathyCoef, true);

        Map<State, List<String>> unfixedAgentHarmPolicy = maxPolicyExtractor.extract(unfixedAgentPolicy, qHarm);

        // Calculate values for good
        Map<State, Map<String, Double>> qGood = calculateQValues(fixedPolicy, unfixedAgentHarmPolicy, unfixedAgent, empathyCoef, false);
        
        Map<State, List<String>> unfixedAgentGoodPolicy = maxPolicyExtractor.extract(unfixedAgentHarmPolicy, qGood);

        Map<State, Map<Agent, String>> newJointPolicy = getJointPolicy(fixedPolicy, unfixedAgentGoodPolicy, unfixedAgent);
        JointPolicy resultJointP = new JointPolicy(newJointPolicy, qHarm, qGood);

        return resultJointP;
    }

    private Map<State, Map<Agent, String>> getJointPolicy(Map<State, Map<Agent, String>> fixedPolicy, Map<State, List<String>> unfixedAgentPolicy, Agent unfixedAgent){
        Map<State, Map<Agent, String>> resultJointPolicy = new HashMap<>(fixedPolicy);
        for(State state : fixedPolicy.keySet()){
            resultJointPolicy.get(state).replace(unfixedAgent, unfixedAgentPolicy.get(state).get(0));
        }
        return resultJointPolicy;
    }

    private Map<State, List<String>> getUnfixedPolicyForAgent(Map<State, Map<Agent, String>> fixedPolicy){
        Map<State, List<String>> unfixedAgentPolicy = new HashMap<>();
        for(State state : fixedPolicy.keySet()){
            unfixedAgentPolicy.put(state, Utility.calculateActionsForState(state));
        }
        return unfixedAgentPolicy;
    }

    private Map<State, List<String>> getPlaceholderUnfixedPolicy(Map<State, Map<Agent, String>> fixedPolicy, Agent unfixedAgent){
        Map<State, List<String>> unfixedAgentPolicy = new HashMap<>();
        for(State state : fixedPolicy.keySet()){
            unfixedAgentPolicy.put(state, Arrays.asList(fixedPolicy.get(state).get(unfixedAgent)));
        }
        return unfixedAgentPolicy;
    }

    // fixedPolicy is the policy for all agents, unfixedAgentPolicy is State -> possibleActions for the unfixed agent 
    public  Map<State, Map<String,  Double>> calculateQValues(Map<State, Map<Agent, String>> fixedPolicy, Map<State, List<String>> unfixedAgentPolicy, Agent unfixedAgent, Double empathyCoef, boolean calculatingHarm){
        // State -> Action -> Value
        Map<State, Map<String,  Double>> qValue = new HashMap<>();

        // Initialise qValue to 0 for all state-action pairs for the unfixed agent
        for(State state : unfixedAgentPolicy.keySet()){
            Map<String, Double> tempActionHarm = new HashMap<>();
            for(String action : unfixedAgentPolicy.get(state)){
                tempActionHarm.put(action, 0.0);
            }
            qValue.put(state, tempActionHarm);
        }

        Double convValue = Double.MAX_VALUE;

        while( convValue > convergenceAchieved){
            
            convValue = 0.0;
            // Looping all states and possible actions for the unfixed agent
            for(State state : unfixedAgentPolicy.keySet()){
                for(String action : unfixedAgentPolicy.get(state)){
                    Double qSumValue = 0.0;
                    Double currentQval;

                    currentQval = qValue.get(state).get(action); 
                    Double transitionProbability;

                    Map<Agent, String> jointAction = new HashMap<>(fixedPolicy.get(state));
                    jointAction.put(unfixedAgent, action);

                    // Loop for all the possible resulting states for calculating the sum 
                    Set<State> successorStates = Utility.calculateSuccessorStates(state, jointAction); 

                    for(State successorState : successorStates){
                        transitionProbability = Utility.transitionFunction(state, jointAction, successorState);
                        
                        Double maxSuccessor = -Double.MAX_VALUE;

                        // Find the max value for the possible actions from the resulting state

                        for(String successorAction : unfixedAgentPolicy.get(successorState)){
                            if(maxSuccessor <= qValue.get(successorState).get(successorAction)){
                                maxSuccessor = qValue.get(successorState).get(successorAction);
                            }
                        }
                        

                        EthicalRewardQuad aggregatedRewardForUnfixedAgent = RewardCalculator.aggregateRewardForAgent(state, jointAction, successorState, allAgents, unfixedAgent, empathyCoef);
                        if(calculatingHarm){
                            // Calculating harm
                            Double causedHarm = aggregatedRewardForUnfixedAgent.getCausedHarm();
                            Double repairedHarm = aggregatedRewardForUnfixedAgent.getRepairedHarm();
    
                            qSumValue += transitionProbability * ((repairedHarm - causedHarm - epsilonCausedHarm * causedHarm ) + gamma * maxSuccessor);
                        }else{
                            // Calculating good
                            Double causedGood = aggregatedRewardForUnfixedAgent.getCausedGood();
                            Double repairedGood = aggregatedRewardForUnfixedAgent.getRepairedGood();
    
                            qSumValue += transitionProbability * ((causedGood - repairedGood - epsilonRepairedGood * repairedGood ) + gamma * maxSuccessor);
                        }

                        
                    }// End of loop for possible resulting states

                    // Set the new values for the state and action

                    qValue.get(state).replace(action, qSumValue);
                    convValue = Math.max(convValue, Math.abs(currentQval - qValue.get(state).get(action)));


                }
            }// End of loop of all states and possible actions
        }// End of while
        return qValue;
    }// End of calculateValues


    public  Map<State, Map<String,  Double>> calculateQValsWithIndReward(Map<State, Map<Agent, String>> fixedPolicy, Map<State, List<String>> evaluatingAgentPolicy, Agent evaluatingAgent, boolean calculatingHarm){
        // State -> Action -> Value
        Map<State, Map<String,  Double>> qValue = new HashMap<>();

        // Initialize qValue to 0 for all state-action pairs for the unfixed agent
        for(State state : evaluatingAgentPolicy.keySet()){
            Map<String, Double> tempActionHarm = new HashMap<>();
            for(String action : evaluatingAgentPolicy.get(state)){
                tempActionHarm.put(action, 0.0);
            }
            qValue.put(state, tempActionHarm);
        }

        Double convValue = Double.MAX_VALUE;

        while( convValue > convergenceAchieved){
            
            convValue = 0.0;
            // Looping all states and possible actions for the unfixed agent
            for(State state : evaluatingAgentPolicy.keySet()){
                for(String action : evaluatingAgentPolicy.get(state)){
                    Double qSumValue = 0.0;
                    Double currentQval;

                    currentQval = qValue.get(state).get(action); 
                    Double transitionProbability;

                    Map<Agent, String> jointAction = new HashMap<>(fixedPolicy.get(state));
                    jointAction.put(evaluatingAgent, action);

                    // Loop for all the possible resulting states for calculating the sum 
                    Set<State> successorStates = Utility.calculateSuccessorStates(state, jointAction); 

                    for(State successorState : successorStates){
                        transitionProbability = Utility.transitionFunction(state, jointAction, successorState);
                        
                        Double maxSuccessor = -Double.MAX_VALUE;

                        // Find the max value for the possible actions from the resulting state

                        for(String successorAction : evaluatingAgentPolicy.get(successorState)){
                            if(maxSuccessor <= qValue.get(successorState).get(successorAction)){
                                maxSuccessor = qValue.get(successorState).get(successorAction);
                            }
                        }
                        
                        // Get the individual reward
                        EthicalRewardQuad individualRewardForEvaluatingAgent = RewardCalculator.individualReward(state, jointAction, successorState, evaluatingAgent);
                        if(calculatingHarm){
                            // Calculating harm
                            Double causedHarm = individualRewardForEvaluatingAgent.getCausedHarm();
                            Double repairedHarm = individualRewardForEvaluatingAgent.getRepairedHarm();
    
                            qSumValue += transitionProbability * ((repairedHarm - causedHarm - epsilonCausedHarm * causedHarm ) + gamma * maxSuccessor);
                        }else{
                            // Calculating good
                            Double causedGood = individualRewardForEvaluatingAgent.getCausedGood();
                            Double repairedGood = individualRewardForEvaluatingAgent.getRepairedGood();
    
                            qSumValue += transitionProbability * ((causedGood - repairedGood - epsilonRepairedGood * repairedGood ) + gamma * maxSuccessor);
                        }

                        
                    }// End of loop for possible resulting states

                    // Set the new values for the state and action

                    qValue.get(state).replace(action, qSumValue);
                    convValue = Math.max(convValue, Math.abs(currentQval - qValue.get(state).get(action)));


                }
            }// End of loop of all states and possible actions
        }// End of while
        return qValue;
    }// End of evaluateWithIndReward

    // QvalueID -> State -> Action -> qValue
    public Map<String, Map<State, Map<String, Double>>> evaluatePolicyWithIndRewardForOneAgent(JointPolicy jointPolicy, Agent unfixedAgent){
        
        Map<String, Map<State, Map<String, Double>>> qValuesForOneAgent =  new HashMap<>();

        Map<State, Map<Agent, String>> fixedPolicy = jointPolicy.getFixedPolicy();

        Map<State, List<String>> unfixedAgentPolicy = getPlaceholderUnfixedPolicy(fixedPolicy, unfixedAgent);

        Map<State, Map<String, Double>> qHarm = calculateQValsWithIndReward(fixedPolicy, unfixedAgentPolicy, unfixedAgent, true);

        Map<State, Map<String, Double>> qGood = calculateQValsWithIndReward(fixedPolicy, unfixedAgentPolicy, unfixedAgent, false);
        
        qValuesForOneAgent.put("H", qHarm);
        qValuesForOneAgent.put("G", qGood);

        return qValuesForOneAgent;
    }

    // Agent -> QvalueID -> State -> Action -> qValue
    public Map<Agent, Map<String, Map<State, Map<String, Double>>>> getIndValuesForAllAgents(JointPolicy jointPolicy){
        
        Map<Agent, Map<String, Map<State, Map<String, Double>>>> qValuesForAllAgents = new HashMap<>();
        
        for(Agent ag : allAgents){
            qValuesForAllAgents.put(ag, evaluatePolicyWithIndRewardForOneAgent(jointPolicy, ag));
        }

        return qValuesForAllAgents;
    }



    public List<Agent> getAllAgents() {
        return allAgents;
    }


    

}
