package src;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import java.util.List;
import java.util.Map;

public class MainHeavy {

    public static void main(String[] args) {

        Map<String, Integer> products = new LinkedHashMap<>();

        Map<String, Integer> context = new HashMap<>();

        context.put("luxury", 1);
        context.put("wastefulness", -1);
        context.put("selfPreservation", -1);

        Map<String, Evaluator> evalMap = new HashMap<>();

        evalMap.put("luxury", new LuxuryEvaluator(1.0));
        evalMap.put("wastefulness", new WastefulnessEvaluator(1.0));
        evalMap.put("selfPreservation", new StarvationEvaluator(1.0));
    
        
        products.put("n0",2);
        products.put("n1", 2);
        
        products.put("l0", 2);

        Map<String, Integer> minDemand = new LinkedHashMap<>();

        minDemand.put("n0",1);
        minDemand.put("n1",1);

        Map<String, Integer> maxDemand = new LinkedHashMap<>();

        maxDemand.put("n0",2);
        maxDemand.put("n1",2);

        
        Agent ag4 = new Agent(4, "H", minDemand, maxDemand, context, evalMap);
        Agent ag5 = new Agent(5, "L", minDemand, maxDemand, context, evalMap);
        Agent ag6 = new Agent(6, "L", minDemand, maxDemand, context, evalMap);
        Agent ag7 = new Agent(7, "L", minDemand, maxDemand, context, evalMap);
        
        List<Agent> allAgentsListOneHeavy = new ArrayList<>(Arrays.asList(ag4, ag5, ag6, ag7));
        
        Map<Agent, Map<String, Integer>> oneHeavyAgentsHave = new LinkedHashMap<>();
        
        for(Agent ag : allAgentsListOneHeavy){
            Map<String, Integer> oneAgentHas = new LinkedHashMap<>();
            for(Map.Entry<String, Integer> agentHas : products.entrySet()){
                oneAgentHas.put(agentHas.getKey(), 0);
            }
            oneHeavyAgentsHave.put(ag, oneAgentHas);
        }
        
        State initialStateOneHeavy = new State(products, oneHeavyAgentsHave);
        
        StochasticGame sgOneHeavy = new StochasticGame(allAgentsListOneHeavy);
        
        Set<State> allStatesOneHeavy = sgOneHeavy.calculateAndReturnAllStates(initialStateOneHeavy);
        // HeavyAgents_LightAgents_NumberOfNecessityQtyOfNecessity_NumberOfLuxuryQtyOfLuxury_EmpathyCoefficient 
        String codeOneHeavy = "1H_3L_2Np3_1Lp2_Em0_05_conv0_1";
	        
        launchXP(allAgentsListOneHeavy, allStatesOneHeavy, initialStateOneHeavy, 0.1, codeOneHeavy);
        
    }

    public static void launchXP(List<Agent> agentsList, Set<State> allStates, State initialState, Double convergenceAchieved, String code){

        Map<String, JointPolicy> policiesMap = new HashMap<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
        
        try{
            File file = new File("results/" + code + ".csv");
            file.createNewFile();

            File fileT = new File("results/" + code + "_timestamps");
            fileT.createNewFile();
            
            FileWriter writer;
            FileWriter writerT;
            
            writer = new FileWriter("results/" + code + ".csv", true);
            writerT = new FileWriter("results/" + code + "_timestamps", true);
            writer.append("HeavyAgents_LightAgents_NumberOfNecessityQtyOfNecessity_NumberOfLuxuryQtyOfLuxury_EmpathyCoefficient ; Lowest agent ; Lowest qHarm ; Highest agent ; Highest qHarm ; Lowest agent ; Lowest qGood ; Highest agent ; Highest qGood ; Nash Soc Welfare Harm ; Nash Soc Welfare Good ; Soc Welfare Harm ; Soc Welfare Good\n");
        	    
            Map<State, Map<Agent, String>> randomPolicy = Utility.getRandomJointPolicy(allStates, agentsList);

            for(Double empathyCoef = 0.0; empathyCoef <= 1.01; empathyCoef+=0.05){

                Map<State, Map<Agent, String>> randomPolicyCopy = Utility.deepCopyRandomPolicy(randomPolicy);
                writerT.append("Started " + code + "@Em:" + empathyCoef  + " at " + dtf.format(LocalDateTime.now())+ "\n");
		        LexVIAlgorithm lexVi = new LexVIAlgorithm(agentsList, 0.9, convergenceAchieved, 0.0, 0.0);
                
                JespAlgorithm jesp = new JespAlgorithm();
                
                JointPolicy jp = jesp.exhaustiveJESP(lexVi, randomPolicyCopy, initialState, empathyCoef);

                policiesMap.put(String.valueOf(empathyCoef), jp);
                // Agent -> QvalueID -> State -> Action -> qValue
                Map<Agent, Map<String, Map<State, Map<String, Double>>>> individualqValuesForAllAgents = lexVi.getIndValuesForAllAgents(jp);

                Map<Agent, Map<String, Double>> indQHarmAtInitialState = new HashMap<>();
                Map<Agent, Map<String, Double>> indQGoodAtInitialState = new HashMap<>();


                for(Map.Entry<Agent, Map<String, Map<State, Map<String, Double>>>> indQvalEntry : individualqValuesForAllAgents.entrySet()){
                    Agent agent = indQvalEntry.getKey();
                    Map<String, Double> valueHarmAtInitialState = indQvalEntry.getValue().get("H").get(initialState);
                    Map<String, Double> valueGoodAtInitialState = indQvalEntry.getValue().get("G").get(initialState);

                    indQHarmAtInitialState.put(agent, valueHarmAtInitialState); 
                    indQGoodAtInitialState.put(agent, valueGoodAtInitialState);
                }

                // Min and max
                AbstractMap.SimpleEntry<Agent,Double> lowestHarm = Utility.getLowestAgentAndValue(indQHarmAtInitialState, jp, initialState);
                Integer lowestHarmAgentNumber = lowestHarm.getKey().getAgentNumber();
                Double lowestHarmValue = lowestHarm.getValue();

                AbstractMap.SimpleEntry<Agent,Double> highestHarm = Utility.getHighestAgentAndValue(indQHarmAtInitialState, jp, initialState);
                Integer highestHarmAgent = highestHarm.getKey().getAgentNumber();
                Double highestHarmValue = highestHarm.getValue();

                AbstractMap.SimpleEntry<Agent,Double> lowestGood = Utility.getLowestAgentAndValue(indQGoodAtInitialState, jp, initialState);
                Integer lowestGoodAgent = lowestGood.getKey().getAgentNumber();
                Double lowestGoodValue = lowestGood.getValue();

                AbstractMap.SimpleEntry<Agent,Double> highestGood = Utility.getHighestAgentAndValue(indQGoodAtInitialState, jp, initialState);
                Integer highestGoodAgent = highestGood.getKey().getAgentNumber();
                Double highestGoodValue = highestGood.getValue();

                // Social Welfare

                Double socialWelfareHarm = Utility.getSocialWelfare(indQHarmAtInitialState, jp, initialState);
                Double socialWelfareGood = Utility.getSocialWelfare(indQGoodAtInitialState, jp, initialState);




                writer.append(code + "Em:" + empathyCoef + ";" + lowestHarmAgentNumber + ";" + lowestHarmValue + ";" + highestHarmAgent + ";" + highestHarmValue + ";");
                writer.append(lowestGoodAgent +";" + lowestGoodValue + ";" + highestGoodAgent + ";" + highestGoodValue + ";");
                writer.append(socialWelfareHarm + ";" + socialWelfareGood+"\n");
                writerT.append("Ended " + code + "@Em:" + empathyCoef + " at " + dtf.format(LocalDateTime.now())+"\n");

            }
            writer.close();
            writerT.close();
        } catch (IOException e) {
            System.out.println("An IO error occurred.");
            e.printStackTrace();
        }

    }
}
