package src;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class StochasticGame{

    private Set<State> allStates = new HashSet<>();

    private List<Agent> agents = new ArrayList<>();


    public StochasticGame(List<Agent> agents){
        this.agents = agents;
        
    }

    public Set<State> calculateAndReturnAllStates(State initialState) {
        Set<State> allPossibleStates = new HashSet<>();
        calculateStatesAux(initialState, allPossibleStates);

        return allPossibleStates;
    }

    private void calculateStatesAux(State currentState, Set<State> allPossibleStates) {
        
        if(allPossibleStates.contains(currentState)){
            return;
        }else{
            allPossibleStates.add(currentState);
            for(Map<Agent, String> jointAction : Utility.generateAllPossibleJointActions(currentState)){
                
                for(State successor : Utility.calculateSuccessorStates(currentState, jointAction)){
                    calculateStatesAux(successor, allPossibleStates);
                }
            }
            

        }
    }

    public Set<State> getAllStates() {
        return allStates;
    }

    public List<Agent> getAgents() {
        return agents;
    }

}