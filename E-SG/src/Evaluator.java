package src;

import java.util.Map;

public interface Evaluator {
    
    public Double evaluate(State state, Map<Agent, String> jointAction, State successorState, Agent agent);
}
