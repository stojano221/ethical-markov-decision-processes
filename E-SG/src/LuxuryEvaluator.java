package src;
import java.util.Map;

public class LuxuryEvaluator implements Evaluator {

    private Double evaluation;

    LuxuryEvaluator(Double evaluation){
        this.evaluation = evaluation;
    }


    @Override
    public Double evaluate(State state, Map<Agent, String> jointAction, State successorState, Agent agent) {
        Double res = 0.0;
        String takenProduct = jointAction.get(agent);
        Integer agentHasProductState = 0;
        Integer agentHasProductSuccessorState = 0;
        boolean productObtained = false;

        if(!takenProduct.equals("NO_PRODUCT")){
            agentHasProductState = state.getAgentsHave().get(agent).get(takenProduct);
            agentHasProductSuccessorState = successorState.getAgentsHave().get(agent).get(takenProduct);
            productObtained = agentHasProductState + 1 == agentHasProductSuccessorState;

        }

        // The agent took a luxury product
        if((takenProduct.charAt(0) == 'l') && productObtained){
            res = evaluation;
        } 
        return res;
    }
}
