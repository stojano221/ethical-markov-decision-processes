package src;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Utility {


    private static final Double productDestructionChance = 0.5;

    // Return possible actions for a state
    public static List<String> calculateActionsForState(State state){
        Map<String, Integer> products = state.getProducts();
        List<String> possibleActions = new ArrayList<>();
        if(!products.values().contains(0)){
            possibleActions.addAll(products.keySet());
        }else{
            for(Map.Entry<String, Integer> product : products.entrySet()){
                if(product.getValue()>0){
                    possibleActions.add(product.getKey());
                }
            }
        }
        possibleActions.add("NO_PRODUCT");
        return possibleActions;
    }

    // Method to obtain combinations of agents and actions
    public static List<Map<Agent, String>> generateAllPossibleJointActions(State s) {
        List<Map<Agent, String>> combinations = new ArrayList<>();
        List<Agent> agents = new ArrayList<>(s.getAgentsHave().keySet());
        List<String> actions = calculateActionsForState(s);
        generateAllPossibleJointActionsAux(agents, actions, new HashMap<>(), combinations);
        return combinations;
    }
    
    // Recursive method to generate combinations
    private static void generateAllPossibleJointActionsAux(List<Agent> agents, List<String> actions,
                                                Map<Agent, String> currentCombination,
                                                List<Map<Agent, String>> combinations) {
        if (currentCombination.size() == agents.size()) {
            // If the combination is complete, add it to the set
            combinations.add(new HashMap<>(currentCombination));
            return;
        }

        Agent nextAgent = new Agent(); 

        for (Agent agent : agents) {
            if (!currentCombination.containsKey(agent)) {
                nextAgent = agent;
            }
        }

        for (String action : actions) {
            currentCombination.put(nextAgent, action);
            generateAllPossibleJointActionsAux(agents, actions, currentCombination, combinations);
            currentCombination.remove(nextAgent);
        }
    }

    // Conflict map where ConflictProduct -> ConflictedAgents
    public static Map<String, Set<Agent>> returnConflictMap(Map<Agent, String> jointAction){

        Map<Agent, String> actionMapCopy = new HashMap<>(jointAction);
        
        // Obtain a set of which products are taken by multiple actions
        Collection<String> actionMapValues = actionMapCopy.values();
        // Set which will contain only one instance of each product
        Set<String> s = new HashSet<>(actionMapValues);
        // Remove the set elements from the action map entries which will only leave the products taken by multiple agents 
        s.forEach(actionMapValues::remove);
        Set<String> duplicateProducts = new HashSet<>(actionMapValues);
        
        // In the case of conflict
        Map<String, Set<Agent>> conflictMap = new HashMap<>();
        if(!duplicateProducts.isEmpty()){
            // Obtain a map of which agents are reaching for the same product
            // Product -> Agents reaching for it    
            for(String dupProduct : duplicateProducts){
                if(!dupProduct.equals("NO_PRODUCT")){
                    Set<Agent> conflictAgents = new HashSet<>();
                    for (Map.Entry<Agent, String> entry : jointAction.entrySet()) {
                        if (entry.getValue().equals(dupProduct)) {
                            conflictAgents.add(entry. getKey());
                        }
                    }
                    conflictAgents.add(new Agent(-1, "DESTR", new HashMap<>(), new HashMap<>()));
                    conflictMap.put(dupProduct, conflictAgents);
                }
            }
        }
        return conflictMap;
    }

    public static Set<State> calculateSuccessorStates(State state, Map<Agent, String> jointAction){
        Set<State> allSuccessorStates = new HashSet<>();

        boolean allAgentsNoAction = true;
        for(Map.Entry<Agent, String> a : jointAction.entrySet()){
            if(!a.getValue().equals("NO_PRODUCT")){
                allAgentsNoAction = false;
            }
        }
        if(!allAgentsNoAction){

            Map<String, Integer> products = state.getProducts();
            Map<Agent, Map<String, Integer>> agentsHave = state.getAgentsHave();

            Map<String, Set<Agent>> conflictMap = new HashMap<>();
            conflictMap = new HashMap<>(returnConflictMap(jointAction));
            
            // Obtain the conflictless actions
            Map<Agent, String> conflictlessActionsCopy = new HashMap<>(jointAction);
            Set<Agent> allConflictedAgents = new HashSet<>();
            for(Map.Entry<String, Set<Agent>> c : conflictMap.entrySet()){
                allConflictedAgents.addAll(c.getValue());
            }
            conflictlessActionsCopy.keySet().removeIf(key -> allConflictedAgents.contains(key));
            
            // Start product attributuion
            Map<String, Integer> conflictlessProducts = new LinkedHashMap<>(products);
            Map<Agent, Map<String, Integer>> conflictlessAgentsHave = new LinkedHashMap<>(agentsHave);
            
            // Conflictless attribution
            for(Map.Entry<Agent, String> conflictlessEntry : conflictlessActionsCopy.entrySet()){
                Agent takingAgent = conflictlessEntry.getKey();
                String takenProduct = conflictlessEntry.getValue();
                
                if(!takenProduct.equals("NO_PRODUCT")){
                    // Attribute product to agent
                    conflictlessProducts.put(takenProduct, products.get(takenProduct)-1);
                    Map<String, Integer> newAgentHas = new LinkedHashMap<>(agentsHave.get(takingAgent));
                    newAgentHas.put(takenProduct, agentsHave.get(takingAgent).get(takenProduct)+1);
                    
                    conflictlessAgentsHave.put(takingAgent, newAgentHas);
                }
                
            }
            
            // Conflicted attribution
            
            if(!conflictMap.isEmpty()){
                Map<String, Set<Agent>> cleanedConflictMap = new HashMap<>();
                for(Map.Entry<String, Set<Agent>> conflictEntry : conflictMap.entrySet()){
                    Set<Agent> resultingConflictAgents = new HashSet<>();
                    resultingConflictAgents.add(new Agent(-1, "DESTR", new HashMap<>(), new HashMap<>()));
                    Set<Agent> heavyAgents = new HashSet<>();
                    Set<Agent> lightAgents = new HashSet<>();
                
                    for(Agent conflictedAgentForProduct : conflictEntry.getValue()){
                        if(conflictedAgentForProduct.getType().equals("H")){
                            heavyAgents.add(conflictedAgentForProduct);
                        }else if(conflictedAgentForProduct.getType().equals("L")){
                            lightAgents.add(conflictedAgentForProduct);
                        }
                    }
                    if(!heavyAgents.isEmpty()){
                        resultingConflictAgents.addAll(heavyAgents);
                    }else{
                        resultingConflictAgents.addAll(lightAgents);
                    }
                    cleanedConflictMap.put(conflictEntry.getKey(), resultingConflictAgents);
                }
                conflictMap = new HashMap<>(cleanedConflictMap);
                // Generate the combinations of agents taking the products
                    Set<Map<Agent, String>> allConflictAttributions = getProductAgentAttributions(conflictMap);
                    List<Map<String, Integer>> conflictProductsList = new ArrayList<>();
                    List<Map<Agent, Map<String, Integer>>> conflictAgentsHaveList = new ArrayList<>();
                    for(Map<Agent, String> attribution : allConflictAttributions){
                        
                        // Create temporary products and agentsHave from the already attributed conflictless ones
                        Map<String, Integer> tempProducts = new LinkedHashMap<>(conflictlessProducts);
                        Map<Agent, Map<String, Integer>> tempAgentsHave = new LinkedHashMap<>(conflictlessAgentsHave);
                        // For each attribution
                        for(Map.Entry<Agent, String> attEntry : attribution.entrySet()){
                            
                            Agent takingAgent = attEntry.getKey();
                            String takenProduct = attEntry.getValue();
                            
                            // If the agent is the "destroying agent" reduce the number of total products for that conflict product and don't attribute products
                            if(takingAgent.getType().equals("DESTR")){
                                if(!takenProduct.equals("NO_PRODUCT")){
                                    tempProducts.put(takenProduct, products.get(takenProduct)-1);
                                }
                            }else{
                                // If the agent is not a "destroying agent" attribute the product
                                if(!takenProduct.equals("NO_PRODUCT")){
                                    tempProducts.put(takenProduct, tempProducts.get(takenProduct)-1);
                                    Map<String,Integer> tempOneAgentHas = new LinkedHashMap<>(tempAgentsHave.get(takingAgent));
                                    tempOneAgentHas.put(takenProduct, tempAgentsHave.get(takingAgent).get(takenProduct)+1);
                                    tempAgentsHave.put(takingAgent,tempOneAgentHas);
                                }
                            }
                        }
                        conflictProductsList.add(tempProducts);
                        conflictAgentsHaveList.add(tempAgentsHave);
                    }// END attribution of conflicted products to agents
                // Create the states for each  couple of the conflict products and agents
                for(int i = 0; i < conflictProductsList.size(); i++){
                    allSuccessorStates.add(new State(conflictProductsList.get(i), conflictAgentsHaveList.get(i)));
                }
            }else{ // END duplicatesPresent
                // If no duplicates are present, add the conflictless attributions
                allSuccessorStates.add(new State(conflictlessProducts, conflictlessAgentsHave));
            }
            
        }else{ // END allAgentsNoAtion
            allSuccessorStates.add(state);
        }
            return allSuccessorStates;
            
    }


    public static Double transitionFunction(State state, Map<Agent, String> jointAction, State successorState){

        Double transitionProbability = 1.0;

        Map<String, Set<Agent>> conflictMap = returnConflictMap(jointAction);

        if(conflictMap.isEmpty()){
            // No conflicts, the probability that the agent obtains the product he reached for is 1
            return transitionProbability;
        }else{
            // If there are conflicts check the states to see which case is it 
            for(Map.Entry<String, Set<Agent>> conflictEntry : conflictMap.entrySet()){
                Double probabilityForOneConflict;
                String conflictProduct = conflictEntry.getKey();
                Set<Agent> conflictedAgentsForProduct = conflictEntry.getValue();

                // Elements of the state
                Map<String, Integer> stateProducts = state.getProducts();
                Map<Agent, Map<String, Integer>> stateAgentsHave = state.getAgentsHave();

                // Elements of the successor state
                Map<String, Integer> successorStateProducts = successorState.getProducts();
                Map<Agent, Map<String, Integer>> successorStateAgentsHave = successorState.getAgentsHave();


                // Has the product quantity decreased for the conflicted product
                boolean productQuantityDecreased = (stateProducts.get(conflictProduct) - 1) == successorStateProducts.get(conflictProduct);
                

                Set<Agent> heavyAgents = new HashSet<>();
                Set<Agent> lightAgents = new HashSet<>();

                for(Agent ag : conflictedAgentsForProduct){

                    // Separate light and heavy agents
                    if(ag.getType().equals("H")){
                        heavyAgents.add(ag);
                    }
                    if(ag.getType().equals("L")){
                        lightAgents.add(ag);
                    }
                }
                boolean productDestroyed = true;

                for(Agent ag : conflictedAgentsForProduct){
                    if(!ag.getType().equals("DESTR")){
                        // An agent has obtained a product
                        if((stateAgentsHave.get(ag).get(conflictProduct) + 1) == successorStateAgentsHave.get(ag).get(conflictProduct)){
                            // Light agent obtained the product and there are no heavy agents: Pr = 1 - destructionPr / card(lightAgentsInConflict)
                            if(ag.getType().equals("L") && heavyAgents.isEmpty()){
                                productDestroyed = false;
                                probabilityForOneConflict = 1.0 - productDestructionChance;
                                probabilityForOneConflict = probabilityForOneConflict / Double.valueOf(lightAgents.size());
                                transitionProbability =  transitionProbability * probabilityForOneConflict;
                                break;
                            }else if(ag.getType().equals("L") && !heavyAgents.isEmpty()){
                                // Light agent obtained the product and there are heavy agents present : Pr = 0
                                transitionProbability = 0.0;
                                break;
                            }else if(ag.getType().equals("H")){
                                // Heavy agent obtained the product: Pr = 1 - destructionPr / card(heavyAgentsinConflict)
                                productDestroyed = false;
                                probabilityForOneConflict = 1.0 - productDestructionChance;
                                probabilityForOneConflict = probabilityForOneConflict / Double.valueOf(heavyAgents.size());
                                transitionProbability =  transitionProbability * probabilityForOneConflict;
                            }
                        }
                        
                    }
                }
                // The quantity for the product has decreased but no agent has obtained it, then it was destroyed
                if(productDestroyed && productQuantityDecreased){
                    
                    transitionProbability = transitionProbability * 0.1;
                }
            }
            
            return transitionProbability;
        }
        
    }
    
        // Method to obtain attributions of products for each agent
    private static Set<Map<Agent, String>> getProductAgentAttributions(Map<String, Set<Agent>> productAgentMap) {
        Set<Map<Agent, String>> attributions = new HashSet<>();
        getProductAgentAttributionsRecursive(productAgentMap, new HashMap<>(), attributions);
        return attributions;
    }

    private static void getProductAgentAttributionsRecursive(
            Map<String, Set<Agent>> remainingProductAgentMap,
            Map<Agent, String> currentAttribution,
            Set<Map<Agent, String>> attributions) {

        if (remainingProductAgentMap.isEmpty()) {
            // If there are no remaining products, add the current attribution to the list
            attributions.add(new HashMap<>(currentAttribution));
            return;
        }

        // Iterate over the products
        for (String product : new HashSet<>(remainingProductAgentMap.keySet())) {
            Set<Agent> agentsForProduct = remainingProductAgentMap.get(product);

            // Iterate over the agents for the current product
            for (Agent agent : new HashSet<>(agentsForProduct)) {
                // Choose the product for the agent
                currentAttribution.put(agent, product);

                // Create a copy of the remaining product-agent map and remove the current product
                Map<String, Set<Agent>> newRemainingProductAgentMap = new HashMap<>(remainingProductAgentMap);
                newRemainingProductAgentMap.remove(product);

                // Remove the chosen agent from the remaining agents for other products
                for (Set<Agent> agents : newRemainingProductAgentMap.values()) {
                    agents.remove(agent);
                }

                // Recursively explore the remaining attributions
                getProductAgentAttributionsRecursive(newRemainingProductAgentMap, currentAttribution, attributions);

                // Backtrack: restore the previous state for the next iteration
                currentAttribution.remove(agent);
            }
        }
    }

    public static Map<State, Map<Agent, String>> deepCopyRandomPolicy(Map<State, Map<Agent, String>> randomPolicy){

        Map<State, Map<Agent, String>> copy = new HashMap<>();
        for(Map.Entry<State, Map<Agent, String>> polEntry : randomPolicy.entrySet()){
            State state = polEntry.getKey();
            Map<Agent, String> agentActionCopy = new HashMap<>();
            for(Map.Entry<Agent,String> agentAction : polEntry.getValue().entrySet()){
                agentActionCopy.put(agentAction.getKey(),agentAction.getValue());
            }
            copy.put(state, agentActionCopy);
        }
        return copy;
    }


    public static Double percentageSimilarity(Map<State, Map<Agent, String>> policyI, Map<State, Map<Agent, String>> policyJ){
        Double baselineCounter = 0.0;
        Double comparisonsCounter = 0.0;
        for(Map.Entry<State, Map<Agent, String>> pIEntry : policyI.entrySet()){

            baselineCounter += Double.valueOf(pIEntry.getValue().size());
            Map<Agent, String> agentActionMapI = pIEntry.getValue();

            for(Map.Entry<Agent, String> agentActionJ : policyJ.get(pIEntry.getKey()).entrySet()){
                String actionI = agentActionMapI.get(agentActionJ.getKey());
                String actionJ = agentActionJ.getValue();
                if(actionJ.equals(actionI)){
                    comparisonsCounter += 1.0;
                }
            }
            
        }
        return comparisonsCounter / baselineCounter;
    }

    public static Map<String, Map<String, Double>> policySimilarityMatrix(Map<String, JointPolicy> policiesMap){
        // empI -> empJ -> similarity
        Map<String, Map<String, Double>> resultSimilarityMatrix = new HashMap<>();

        for(Map.Entry<String, JointPolicy> empPolicyI : policiesMap.entrySet()){
            String empathyCoefI = empPolicyI.getKey();
            Map<State, Map<Agent, String>> policyI = empPolicyI.getValue().getFixedPolicy();

            // empJ -> similarity
            Map<String, Double> policySimInner = new HashMap<>();

            for(Map.Entry<String, JointPolicy> empPolicyJ : policiesMap.entrySet()){
                String empathyCoefJ = empPolicyJ.getKey();
                Map<State, Map<Agent, String>> policyJ = empPolicyJ.getValue().getFixedPolicy();

                policySimInner.put(empathyCoefJ, percentageSimilarity(policyI, policyJ));
            }

            resultSimilarityMatrix.put(empathyCoefI, policySimInner);
        }

        return resultSimilarityMatrix;
    }

    public static AbstractMap.SimpleEntry<Agent,Double> getLowestAgentAndValue(Map<Agent, Map<String, Double>> agentsQValueAtInitialState, JointPolicy jp, State initialState){
        
        Double min = Double.MAX_VALUE;
        Agent minAgent = new Agent();

        for(Map.Entry<Agent, Map<String, Double>> agentQval : agentsQValueAtInitialState.entrySet()){
            Double qValue = agentQval.getValue().get(jp.getFixedPolicy().get(initialState).get(agentQval.getKey()));
            if(min > Math.abs(qValue)){
                min = Math.abs(qValue);
                minAgent = agentQval.getKey();
                
            }
        }
        return new SimpleEntry<Agent,Double>(minAgent, min);
    }

    public static AbstractMap.SimpleEntry<Agent,Double> getHighestAgentAndValue(Map<Agent, Map<String, Double>> agentsQValueAtInitialState, JointPolicy jp, State initialState){
        
        Double max = -Double.MAX_VALUE;
        Agent maxAgent = new Agent();
        
        for(Map.Entry<Agent, Map<String, Double>> agentQval : agentsQValueAtInitialState.entrySet()){
            Double qValue = agentQval.getValue().get(jp.getFixedPolicy().get(initialState).get(agentQval.getKey()));
            if(max < Math.abs(qValue)){
                max = Math.abs(qValue);
                maxAgent = agentQval.getKey();
            }
        }
        return new SimpleEntry<Agent,Double>(maxAgent, max);
    }

    public static Double getNashSocialWelfare(Map<Agent, Map<String, Double>> agentsQValueAtInitialState, JointPolicy jp, State initialState){
        
        Double product = 1.0;

        for(Map.Entry<Agent, Map<String, Double>> agentQval : agentsQValueAtInitialState.entrySet()){
            Agent agent = agentQval.getKey();
            Map<String, Double> actionQValAtInitialState = agentQval.getValue();
            Double qValAtInitialState = actionQValAtInitialState.get(jp.getFixedPolicy().get(initialState).get(agent));

            product *= 1 + Math.abs(qValAtInitialState);
        }

        return product;
    }

    public static Double getSocialWelfare(Map<Agent, Map<String, Double>> agentsQValueAtInitialState, JointPolicy jp, State initialState){
        
        Double sum = 0.0;

        for(Map.Entry<Agent, Map<String, Double>> agentQval : agentsQValueAtInitialState.entrySet()){
            Agent agent = agentQval.getKey();
            Map<String, Double> actionQValAtInitialState = agentQval.getValue();
            Double qValAtInitialState = actionQValAtInitialState.get(jp.getFixedPolicy().get(initialState).get(agent));

            sum += Math.abs(qValAtInitialState);
        }

        return sum;
    }

    public static Map<State, Map<Agent, String>> getRandomJointPolicy(Set<State> allStates, List<Agent> agents){
        Map<State, Map<Agent, String>> randomPolicy = new HashMap<>();
        Random rand = new Random();

        for(State state : allStates){
            Map<Agent, String> agentActionPairs = new HashMap<>();
            for(Agent agent : agents){
                List<String> possibleActions = Utility.calculateActionsForState(state);

                agentActionPairs.put(agent, possibleActions.get(rand.nextInt(possibleActions.size())));
                

            }
            randomPolicy.put(state, agentActionPairs);
        }
        
        return randomPolicy;
    }

}
