Whiele in the EMDP folder:

- To compile:
javac -cp lib/*:. -d build src/*.java src/agents/*.java src/factories/*.java src/parsers/*.java src/rewards/*.java src/states/*.java src/valueIterationAlgorithms/*.java src/worlds/*.java


- To execute:
java -cp lib/*:build Main ARG

- Change the argument according to the desired results:
ARG = 0 - DCT framework
      1 - PFD framework
      2 - VE framework
      3 - All frameworks
      
Compiled and executed with Java version: openjdk version "17.0.12" 2024-07-16
