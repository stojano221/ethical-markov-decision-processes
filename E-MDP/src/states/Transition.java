package states;

public class Transition {
    private String state;
    private String action;
    private String successor;

    public Transition(String state, String action, String successor){
        this.state = state;
        this.action = action;
        this.successor = successor;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setSuccessor(String successor) {
        this.successor = successor;
    }

    public String getAction() {
        return action;
    }

    public String getState() {
        return state;
    }

    public String getSuccessor() {
        return successor;
    }

    @Override
    public String toString() {
        String res = "State: "  + state + "\n ";
        res += "Action: "    + action+ "\n ";
        res+= "Successor: "  + successor+ "\n ";
        return res;
    }

}
