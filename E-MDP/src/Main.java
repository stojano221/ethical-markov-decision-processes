import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.util.Map.entry;

import valueIterationAlgorithms.SuccessiveValueIteration;
import agents.SelfDrivingCarAgent;
import factories.DCTFactory;
import factories.PFDFactory;
import factories.StateProfile;
import factories.VEFactory;
import factories.VirtueEthicsData;
import parsers.WorldMapParser;
import rewards.RewardCalculator;
import worlds.SelfDrivingCarWorld;

public class Main {
    public static void main(String[] args) {


        // Parsing map to set up the world, and setting start and goal locations
        WorldMapParser parser = new WorldMapParser();
        SelfDrivingCarWorld parsedWorld = parser.getWorldFromJsonMap("maps/SelfDrivingCarMap.json");

        if(args.length == 0){
            System.out.println( "Invalid command line argument!\n" + 
            "run with : 0 for DCT, 1 for PFD, 2 for VE or 3 for all at once");
        }else{
            switch (args[0]) {
                case "0":
                    
                    launchDCT(parsedWorld);
                    break;
                case "1":
                    launchPFD(parsedWorld);
                    break;
                case "2":
                    launchVE(parsedWorld);
                    break;
                case "3":
                    launchDCT(parsedWorld);
                    launchPFD(parsedWorld);
                    launchVE(parsedWorld);
                    break;
                default:
                    System.out.println( "Invalid command line argument!\n" + 
                                        "run with : 0 for DCT, 1 for PFD, 2 for VE or 3 for all at once");
            }
        }


    }

    public static void launchDCT(SelfDrivingCarWorld world){
        List<Integer> contextDCT = new ArrayList<>();
        contextDCT.add(0);
        
        // Forbidden state profiles for DCT as explained in the original paper Hazardous(H) and Inconsiderate(I)
        StateProfile hazardous = new StateProfile("ALL", "ALL", "HIGH", "ALL", false);
        List<StateProfile> hazardousList = new ArrayList<>(Arrays.asList(hazardous));
        Map<Integer, List<StateProfile>> profileMapHazardous = Collections.singletonMap(0, hazardousList);
        
        
        StateProfile inconsiderate = new StateProfile("ALL", "ALL", "NORMAL", "HEAVY", false);
        List<StateProfile> hazardousInconsiderateProfileList = new ArrayList<>(Arrays.asList(hazardous,inconsiderate));
        Map<Integer, List<StateProfile>> profileMapHazardousAndInconsiderate = Collections.singletonMap(0, hazardousInconsiderateProfileList);

        // Inconsiderate
        List<StateProfile> inconsiderateProfileList = new ArrayList<>(Arrays.asList(inconsiderate));
        Map<Integer, List<StateProfile>> profileMapInconsiderate = Collections.singletonMap(0, inconsiderateProfileList);
        
        
        DCTFactory dctFactory = new DCTFactory(world, profileMapHazardousAndInconsiderate);
        dctFactory.createEvaluations();
        
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsDCTHI = new HashMap<>();
        transitionEvalsDCTHI.put(0,dctFactory.getTransitionEvalForContextIndex(0));
        
        dctFactory.clearTransitionEvals();
        dctFactory.setForbidden(profileMapHazardous);
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsDCTH = new HashMap<>();
        dctFactory.createEvaluations();
        transitionEvalsDCTH.put(0,dctFactory.getTransitionEvalForContextIndex(0));

        dctFactory.clearTransitionEvals();
        dctFactory.setForbidden(profileMapInconsiderate);
        dctFactory.createEvaluations();

        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsDCTI = new HashMap<>();
        transitionEvalsDCTI.put(0,dctFactory.getTransitionEvalForContextIndex(0));
        
        dctFactory.clearTransitionEvals();
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsDCTBlank = new HashMap<>(dctFactory.getBlankTransitionEvals(1));

        System.out.println("---------------------------------- DCT ----------------------------------");
        BufferedWriter writerDCT;
        try {
            writerDCT = new BufferedWriter(new FileWriter("results/DCTresults.txt", false));
            
            // Hazardous

            writerDCT.append("Hazardous \n");
            writerDCT.append("Hazardous DCT for task 1: " + "\n");
            world.setStartLocation("SCHOOL");
            world.setGoalLocation("DINER");
            showLoss(contextDCT, world, transitionEvalsDCTH, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Hazardous DCT for task 2: " + "\n");
            world.setStartLocation("HOME");
            world.setGoalLocation("OFFICE");
            showLoss(contextDCT, world, transitionEvalsDCTH, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Hazardous DCT for task 3: " + "\n");
            world.setStartLocation("TOWN_HALL");
            world.setGoalLocation("PARK");
            showLoss(contextDCT, world, transitionEvalsDCTH, transitionEvalsDCTBlank, writerDCT);

            // Inconsiderate

            writerDCT.append("\nInconsiderate\n");
            writerDCT.append("Inconsiderate DCT for task 1: " + "\n");
            world.setStartLocation("SCHOOL");
            world.setGoalLocation("DINER");
            showLoss(contextDCT, world, transitionEvalsDCTI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Inconsiderate DCT for task 2: " + "\n");
            world.setStartLocation("HOME");
            world.setGoalLocation("OFFICE");
            showLoss(contextDCT, world, transitionEvalsDCTI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Inconsiderate DCT for task 3: " + "\n");
            world.setStartLocation("TOWN_HALL");
            world.setGoalLocation("PARK");
            showLoss(contextDCT, world, transitionEvalsDCTI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            // Hazardous and Inconsiderate

            writerDCT.append("\nHazardous & Inconsiderate\n");
            writerDCT.append("Hazardous & Inconsiderate DCT for task 1: " + "\n");
            world.setStartLocation("SCHOOL");
            world.setGoalLocation("DINER");
            showLoss(contextDCT, world, transitionEvalsDCTHI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Hazardous & Inconsiderate DCT for task 2: " + "\n");
            world.setStartLocation("HOME");
            world.setGoalLocation("OFFICE");
            showLoss(contextDCT, world, transitionEvalsDCTHI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.append("Hazardous & Inconsiderate DCT for task 3: " + "\n");
            world.setStartLocation("TOWN_HALL");
            world.setGoalLocation("PARK");
            showLoss(contextDCT, world, transitionEvalsDCTHI, transitionEvalsDCTBlank, writerDCT);

            writerDCT.append("\n");

            writerDCT.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    public static void launchPFD(SelfDrivingCarWorld world){
        // PFD definitions of duties
        
        List<Integer> contextPFD = new ArrayList<>();
        contextPFD.add(0);
        
        
        StateProfile smoothOperationState = new StateProfile("ALL", "ALL", "NONE", "LIGHT",false);
        String smoothOperationAction = "TO_HIGH || TO_NORMAL";
        Map<StateProfile, String> smoothOperationDuty = new HashMap<>();
        smoothOperationDuty.put(smoothOperationState, smoothOperationAction);
        
        StateProfile carefulOperationState = new StateProfile("ALL", "ALL", "NONE", "HEAVY",false);
        String carefulOperationAction = "TO_LOW";
        Map<StateProfile, String> carefulOperationDuty = new HashMap<>();
        carefulOperationDuty.put(carefulOperationState, carefulOperationAction);

        List<Map<StateProfile, String>> dutiesList = new ArrayList<>(Arrays.asList(smoothOperationDuty, carefulOperationDuty));
        
        Map<Integer, List<Map<StateProfile, String>>> contextIndexToDutiesList = new HashMap<>();
        contextIndexToDutiesList.put(0,dutiesList);
        
        PFDFactory pfdFactory = new PFDFactory(world, contextIndexToDutiesList);
        pfdFactory.createEvaluations();
        
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsPFD = pfdFactory.getTransitionEval();
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsPFDBlank = pfdFactory.getBlankTransitionEvals(1);


        System.out.println("---------------------------------- PFD ----------------------------------");
        BufferedWriter writerPFD;
        try {
            writerPFD = new BufferedWriter(new FileWriter("results/PFDresults.txt", false));

            writerPFD.append("0 tolerance PFD for task 1: " + "\n");
            world.setStartLocation("SCHOOL");
            world.setGoalLocation("DINER");
            showLoss(contextPFD, world, transitionEvalsPFD, transitionEvalsPFDBlank, writerPFD);

            writerPFD.append("\n");

            writerPFD.append("0 tolerance PFD for task 2: " + "\n");
            world.setStartLocation("HOME");
            world.setGoalLocation("OFFICE");
            showLoss(contextPFD, world, transitionEvalsPFD, transitionEvalsPFDBlank, writerPFD);

            writerPFD.append("\n");

            writerPFD.append("0 tolerance PFD for task 3: " + "\n");
            world.setStartLocation("TOWN_HALL");
            world.setGoalLocation("PARK");
            showLoss(contextPFD, world, transitionEvalsPFD, transitionEvalsPFDBlank, writerPFD);

            writerPFD.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    public static void launchVE(SelfDrivingCarWorld world){
            

        System.out.println("---------------------------------- VE ----------------------------------");
        BufferedWriter writerVE;
        try {
            writerVE = new BufferedWriter(new FileWriter("results/VEresults.txt", false));
            smallAndLargeLaunchVE(world, "SCHOOL", "DINER",writerVE);
            smallAndLargeLaunchVE(world, "HOME", "OFFICE", writerVE);
            smallAndLargeLaunchVE(world, "TOWN_HALL", "PARK", writerVE);

            writerVE.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

            

    }

    public static void smallAndLargeLaunchVE(SelfDrivingCarWorld world, String startLocation, String goalLocation, BufferedWriter writerVE){
         // Proactive: 
        
        // Avoid the Highway state

        StateProfile avoidHighwayState = new StateProfile("ALL", "HIGHWAY", "NONE", "ALL", false);
        String avoidHighwayAction = "TO_LOW || TO_NORMAL || TO_HIGH";
        StateProfile avoidHighwaySuccessorState = new StateProfile("ALL", "HIGHWAY", "ALL", "ALL",false);

        VirtueEthicsData avoidHighwayData = new VirtueEthicsData(avoidHighwayState, avoidHighwayAction, avoidHighwaySuccessorState);

        // Avoid the School state
        StateProfile avoidSchoolState1 = new StateProfile("MATOON_STREET_REVERSED", "ALL", "ALL", "ALL", false);
        String avoidSchoolAction1 = "CRUISE";
        StateProfile avoidSchoolSuccessorState1 = new StateProfile("SCHOOL", "ALL", "ALL", "ALL",true);

        VirtueEthicsData avoidShoolData1 = new VirtueEthicsData(avoidSchoolState1, avoidSchoolAction1, avoidSchoolSuccessorState1);

        StateProfile avoidSchoolState2 = new StateProfile("ASTOR_DRIVE_REVERSED", "ALL", "ALL", "ALL", false);
        String avoidSchoolAction2 = "CRUISE";
        StateProfile avoidSchoolSuccessorState2 = new StateProfile("SCHOOL", "ALL", "ALL", "ALL",true);

        VirtueEthicsData avoidShoolData2 = new VirtueEthicsData(avoidSchoolState2, avoidSchoolAction2, avoidSchoolSuccessorState2);

        StateProfile avoidShoolState3 = new StateProfile("TRIANGLE_STREET_REVERSED", "ALL", "ALL", "ALL", false);
        String avoidShoolAction3 = "CRUISE";
        StateProfile avoidShoolSuccessorState3 = new StateProfile("SCHOOL", "ALL", "ALL", "ALL",true);

        VirtueEthicsData avoidShoolData3 = new VirtueEthicsData(avoidShoolState3, avoidShoolAction3, avoidShoolSuccessorState3);


        // Cautious, light -> to normal, heavy -> to low

        StateProfile trajectoryStateC1Large = new StateProfile("ALL", "ALL", "NONE", "LIGHT",false);
        String trajectoryActionC1Large = "TO_NORMAL";
        StateProfile trajectorySuccessorStateC1Large = new StateProfile("ALL", "ALL", "NORMAL", "LIGHT",false);

        VirtueEthicsData cautious_1Large = new VirtueEthicsData(trajectoryStateC1Large, trajectoryActionC1Large, trajectorySuccessorStateC1Large);

        StateProfile trajectoryStateC2Large = new StateProfile("ALL", "ALL", "NONE", "HEAVY",false);
        String trajectoryActionC2Large = "TO_LOW";
        StateProfile trajectorySuccessorStateC2Large = new StateProfile("ALL", "ALL", "LOW", "HEAVY",false);

        
        VirtueEthicsData cautious_2Large = new VirtueEthicsData(trajectoryStateC2Large, trajectoryActionC2Large, trajectorySuccessorStateC2Large);
        
        StateProfile goalState = new StateProfile(goalLocation, "ALL", "ALL", "ALL",true);
        String goalAction = "STAY";
        StateProfile goalState2 = new StateProfile(goalLocation, "ALL", "ALL", "ALL",true);
        VirtueEthicsData goalData = new VirtueEthicsData(goalState, goalAction, goalState2);
        // Change the goal data as each goal changes, make a method that takes a string and does the changes we need for the world as well


        List<VirtueEthicsData> dataListVEproactive = new ArrayList<>(Arrays.asList(avoidHighwayData, avoidShoolData1, avoidShoolData2, avoidShoolData3));
        List<VirtueEthicsData> dataListVEcautious = new ArrayList<>(Arrays.asList(cautious_1Large, cautious_2Large, goalData));
        
        List<Integer> contextVEsmall = new ArrayList<>(Arrays.asList(0,1));
        Map<Integer, List<VirtueEthicsData>> dataMapVESmall = Collections.singletonMap(0, dataListVEcautious);

        VEFactory veFactory = new VEFactory(world);
        veFactory.createPositiveEvaluations(dataMapVESmall);
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVEsmall = veFactory.getTransitionEval();
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVEsmallBlank = veFactory.getBlankTransitionEvals(2);

        try {            
            // Small
            
            writerVE.append("Small VE trajectory for start location [" +startLocation +"] and goal location [" + goalLocation  + "]\n");
            world.setStartLocation(startLocation);
            world.setGoalLocation(goalLocation);
            showLoss(contextVEsmall, world, transitionEvalsVEsmall, transitionEvalsVEsmallBlank, writerVE);

        } catch (IOException e) {
            e.printStackTrace();
        }

        veFactory.clearTransitionEvals();

        // VE-P
        /*List<Integer> contextVEproactive = new ArrayList<>(Arrays.asList(0,1));

        Map<Integer, List<VirtueEthicsData>> dataMapProactive2 =   new HashMap<>(Map.ofEntries((entry(0, dataListVEproactive))));
        veFactory.createNegativeTrajectoryEvals(dataMapProactive2);
        
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVEproactive = new HashMap<>();
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVEproactiveBlank = veFactory.getBlankTransitionEvals(2);

        transitionEvalsVEproactive.put(0,veFactory.getTransitionEvalForContextIndex(0));
        transitionEvalsVEproactive.put(1,veFactory.getTransitionEvalForContextIndex(1));

        

            // Proactive
        // try {
        //     writerVE.append("\n Proactive VE trajectory for start location [" +startLocation +"] and goal location [" + goalLocation  + "]\n");
        //     world.setStartLocation(startLocation);
        //     world.setGoalLocation(goalLocation);
        //     showLoss(contextVEproactive, world, transitionEvalsVEproactive, transitionEvalsVEproactiveBlank, writerVE);

            
        // } catch (IOException e) {
        //     e.printStackTrace();
        // }
        */

        veFactory.clearTransitionEvals();

        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVElarge = new HashMap<>();
        Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsVElargeBlank = veFactory.getBlankTransitionEvals(4);

        List<Integer> contextVElarge = new ArrayList<>(Arrays.asList(0,1, 0,1));
        Map<Integer, List<VirtueEthicsData>> dataMapCautious =   new HashMap<>(Map.ofEntries((entry(0, dataListVEcautious))));
        veFactory.createPositiveEvaluations(dataMapCautious);
        transitionEvalsVElarge.put(0,veFactory.getTransitionEvalForContextIndex(0));
        transitionEvalsVElarge.put(1,veFactory.getTransitionEvalForContextIndex(1));

        Map<Integer, List<VirtueEthicsData>> dataMapProactive =   new HashMap<>(Map.ofEntries((entry(2, dataListVEproactive))));
        veFactory.createNegativeTrajectoryEvals(dataMapProactive);

        transitionEvalsVElarge.put(2,veFactory.getTransitionEvalForContextIndex(2));
        transitionEvalsVElarge.put(3,veFactory.getTransitionEvalForContextIndex(3));

        try {
            // Large
            writerVE.append("\nLarge VE trajectory for start location [" +startLocation +"] and goal location [" + goalLocation  + "]\n");
            world.setStartLocation(startLocation);
            world.setGoalLocation(goalLocation);
            showLoss(contextVElarge, world, transitionEvalsVElarge, transitionEvalsVElargeBlank, writerVE);

            writerVE.append("\n-------------------------------------------------\n\n");

            
        } catch (IOException e) {
            e.printStackTrace();
        }

        

    }

    public static void showLoss(List<Integer> context, 
                                SelfDrivingCarWorld parsedWorld, 
                                Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsEthics, 
                                Map<Integer, Map<String, Map<String, Map<String, Integer>>>> transitionEvalsBlank, 
                                BufferedWriter writer){

        RewardCalculator rewardCalculatorEthics = new RewardCalculator(context, transitionEvalsEthics, parsedWorld);
        RewardCalculator rewardCalculatorBlank = new RewardCalculator(context, transitionEvalsBlank, parsedWorld);
            
        SelfDrivingCarAgent agentEthics = new SelfDrivingCarAgent(parsedWorld, rewardCalculatorEthics);
        SelfDrivingCarAgent agentBlank = new SelfDrivingCarAgent(parsedWorld, rewardCalculatorBlank);
        
        long startTime0 = System.nanoTime();
        SuccessiveValueIteration successiveValueIteration = new SuccessiveValueIteration(agentEthics, parsedWorld, 0.001, 0.99, 1. , 1. , 1., 1.);
        long endTime0 = System.nanoTime();
        System.out.println("Time Elapsed: " + (endTime0 - startTime0) + "ns");
        
        Map<String, List<String>> successivePolicyEthics = successiveValueIteration.calculatePolicyForSuccessiveValueIteration();

        SuccessiveValueIteration successiveValueIterationBlank = new SuccessiveValueIteration(agentBlank, parsedWorld, 0.001, 0.99, 1. , 1. , 1., 1.);
        Map<String, List<String>> successivePolicyBlank = successiveValueIterationBlank.calculatePolicyForSuccessiveValueIteration();
        
        Map<String, Double> vHarmGoodTaskEthics = successiveValueIteration.calculateAndReturnVforPolicy(successivePolicyEthics);
        Map<String, Double> vTaskBlank = successiveValueIterationBlank.calculateAndReturnVforPolicy(successivePolicyBlank);

        Double maxDiff = -Double.MAX_VALUE;
        String diffState = "";
        for(String state : parsedWorld.getAllStateKeys()){
            if(maxDiff < vHarmGoodTaskEthics.get(state) - vTaskBlank.get(state)){
                maxDiff = vHarmGoodTaskEthics.get(state) - vTaskBlank.get(state);
                diffState = state;
            }
        }


        try {
            writer.append("Most difference in state: TASK[" + diffState +"]= " + vTaskBlank.get(diffState) + "; Ethics["+ diffState + "]= " + vHarmGoodTaskEthics.get(diffState) + "\n");

            writer.append("Highest difference value: " + maxDiff + "\n");

            Double lossDCT = (maxDiff / vTaskBlank.get(diffState)) * 100;
            writer.append("Highest loss: " + lossDCT + "%\n");
    
            writer.append("Values at start location: TASK[" + parsedWorld.getStartLocation() + "]=" + vTaskBlank.get(parsedWorld.getStartLocation()) + "; Ethics["+parsedWorld.getStartLocation()+"]="+vHarmGoodTaskEthics.get(parsedWorld.getStartLocation()) + "\n");
            Double dif = vHarmGoodTaskEthics.get(parsedWorld.getStartLocation()) - vTaskBlank.get(parsedWorld.getStartLocation());
            writer.append("Difference at start location:" + dif + "\n");
            Double lossAtStart = (dif / vTaskBlank.get(parsedWorld.getStartLocation())) * 100;
            writer.append("Loss at start location: " + lossAtStart + "%\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}